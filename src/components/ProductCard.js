import React from "react";
import PropTypes from 'prop-types';

function ProductCard({ cardData, buttons }) {
  const { name, url, color, price, article } = cardData;

  return (
    <li className="product__item">
      <img className="product__img" alt={name} src={url} />
      <h3 className="product__name">{name}</h3>
      <div className="product__card-layout">
        <div className="product__info">
          <p className="product__color">{color}</p>
          <h3 className="product__price">€ {price}</h3>
          <p className="product__article">{article}</p>
        </div>
        <div className="product__buttons">{buttons}</div>
      </div>
    </li>
  );
}

ProductCard.propTypes = {
    cardData: PropTypes.shape({
      key: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      article: PropTypes.number.isRequired,
    }).isRequired,
    buttons: PropTypes.element,
  };

  ProductCard.defaultProps = {
    buttons: null,
  };

export default ProductCard;
