import React from "react";
import PropTypes from 'prop-types';

function CartItem({ cardData, buttons }) {
  const { name, url, color, price, article, quantity } = cardData;



  return (
    <li className="cart-item">
      <img className="cart-item__img" alt={name} src={url} />
      <div className="cart-item__layout">
        <h3 className="cart-item__name">{name}</h3>
        <div className="cart-item__info">
          <p className="cart-item__color">{color}</p>
          <p className="cart-item__article">{article}</p>
        </div>
        <h3 className="cart-item__quantity">{quantity} pcs.</h3>
        <h3 className="cart-item__price">€ {price}</h3>
        <h3 className="cart-item__total-price">€ {quantity * price}</h3>
        <div className="cart-item__buttons">{buttons}</div>
      </div>
    </li>
  );
}

CartItem.propTypes = {
    cardData: PropTypes.shape({
      key: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      article: PropTypes.number.isRequired,
      quantity: PropTypes.number.isRequired,
    }).isRequired,
    buttons: PropTypes.element,
  };

  CartItem.defaultProps = {
    buttons: null,
  };

export default CartItem;