import React from "react";
import { IoStarOutline, IoStarSharp } from "react-icons/io5";
import PropTypes from "prop-types";

function Star({ clickFunc, type = "Empty" }) {
  return (
    <button className="star" onClick={clickFunc}>
      {type === "Empty" && <IoStarOutline />}
      {type === "Painted" && <IoStarSharp />}
    </button>
  );
}

Star.propTypes = {
  clickFunc: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};

export default Star;
