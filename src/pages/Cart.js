import React, { useState, useEffect } from "react";
import Button from "../components/Button";
import Modal from "../components/Modal";
import CartItem from "../components/CartItem";

const Cart = () => {
  const [itemsInCart, setitemsInCart] = useState([]);
  const [openedModal, setOpenModal] = useState("None");
  const [activeProduct, setActiveProduct] = useState("");

  const checkActiveProduct = (product) => {
    setActiveProduct(product);
  };

  const openModal = (modal) => {
    setOpenModal(modal);
  };

  const openDeleteModal = (product) => {
    openModal("Delete");
    checkActiveProduct(product);
  };

  const getProductsFromCartArray = () => {
    return JSON.parse(localStorage.getItem("productsInCart"));
  }

  const removeProductFromCart = (product) => {

    const productCartArray = getProductsFromCartArray();

    const newProductCartArray = productCartArray.filter(
        (productFromArray) => productFromArray.key !== product.key
      );

      localStorage.setItem(
        "productsInCart",
        JSON.stringify(newProductCartArray)
      );

      setOpenModal("None");
      setActiveProduct("");
  }

  const calculateTotal = () => {
    const productCartArray = getProductsFromCartArray();

    let totalSum = 0;

    productCartArray.forEach(product => {
        totalSum = totalSum + (product.quantity * product.price)
    });
    return totalSum;
  }


  useEffect(() => {
    setitemsInCart(() => [...getProductsFromCartArray()]);
  }, [removeProductFromCart]);








  return (
    <>
      <ul className="cart-container">
        {itemsInCart.map((product) => (
          <CartItem
            cardData={product}
            buttons={
              <>
                <Button clickFunc={() => openDeleteModal(product)} text="X" />
              </>
            }
          />
        ))}
      </ul>
      <div className="cart-total">Total: € {calculateTotal()}</div>

      {openedModal === "Delete" && (
        <Modal
          modalSetter={setOpenModal}
          header="Delete?"
          closeButton={true}
          text="Are you sure you want to remove this product from your cart?"
          actions={
            <>
              <Button clickFunc={() => removeProductFromCart(activeProduct)} text="Yes" />
              <Button clickFunc={() => setOpenModal("None")} text="No" />
            </>
          }
        />
      )}
    </>
  );
};

export { Cart };
